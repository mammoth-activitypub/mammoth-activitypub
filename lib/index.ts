import {Actor as ASActor, Announce, ASObject, CollectionRef, Dislike, Like, LinkRef} from 'mammoth-activitystreams';

// @ts-ignore
export interface Actor extends APObject, ASActor {
    inbox: LinkRef;
    outbox: LinkRef;
    following?: LinkRef;
    followers?: LinkRef;
    liked?: LinkRef;
    streams?: LinkRef;
    preferredUsername?: string;
    endpoints?: string | Map<string, LinkRef>;
    proxyUrl?: string;
    oathAuthorizationEndpoint?: string;
    oauthTokenEndpoint?: string;
    provideClientKey?: string;
    signClientKey?: string;
    sharedInbox?: string;
    publicKey: {
        id: string;
        owner: string;
        publicKeyPem: string;
    };
}

'use strict';

export interface APObject extends ASObject {
    likes?: CollectionRef<Like>;
    shares?: CollectionRef<Announce>;
    dislikes?: CollectionRef<Dislike>;
}
